/* Tailwind - The Utility-First CSS Framework */

/* Config */

const colors = {
  transparent: 'transparent',
  black: '#000',
  white: '#fff'
}

const spacing = {
  px: '1px'
}

const sizing = {
  auto: 'auto',
  px: '1px'
}

module.exports = {
  colors,

  screens: {
    sm: '576px',
    md: '768px',
    lg: '992px',
    xl: '1200px'
  },

  textSizes: {
    base: '14px'
  },

  fontWeights: {
    hairline: 100,
    thin: 200,
    light: 300,
    normal: 400,
    medium: 500,
    semibold: 600,
    bold: 700,
    extrabold: 800,
    black: 900
  },

  leading: {
    none: 1,
    tight: 1.25,
    normal: 1.5,
    loose: 2
  },

  tracking: {
    tight: '-0.05em',
    normal: '0',
    wide: '0.05em'
  },

  textColors: colors,

  backgroundColors: colors,

  backgroundSize: {
    auto: 'auto',
    cover: 'cover',
    contain: 'contain'
  },

  borderWidths: {
    default: '1px',
    0: '0'
  },

  borderColors: global.Object.assign({
    default: 'currentColor'
  }, colors),

  borderRadius: {
    default: '0.25rem',
    full: '9999px'
  },

  width: global.Object.assign({
    '1/2': '50%',
    '1/3': '33.33333%',
    '2/3': '66.66667%',
    '1/4': '25%',
    '3/4': '75%',
    '1/5': '20%',
    '2/5': '40%',
    '3/5': '60%',
    '4/5': '80%',
    '1/6': '16.66667%',
    '5/6': '83.33333%',
    full: '100%',
    screen: '100vw'
  }, sizing),

  height: global.Object.assign({
    full: '100%',
    screen: '100vh'
  }, sizing),

  minWidth: {
    0: '0',
    full: '100%'
  },

  minHeight: {
    0: '0',
    full: '100%',
    screen: '100vh'
  },

  maxWidth: {
    full: '100%'
  },

  maxHeight: {
    full: '100%',
    screen: '100vh'
  },

  padding: global.Object.assign({}, spacing),

  margin: global.Object.assign({
    auto: 'auto'
  }, spacing),

  negativeMargin: global.Object.assign({}, spacing),

  shadows: {
    default: '0 2px 4px 0 rgba(0, 0, 0, 0.10)',
    none: 'none'
  },

  zIndex: {
    auto: 'auto',
    0: 0,
    10: 10,
    20: 20,
    30: 30,
    40: 40,
    50: 50
  },

  opacity: {
    0: '0',
    25: '0.25',
    50: '0.5',
    75: '0.75',
    100: '1'
  },

  options: {
    prefix: '',
    important: false,
    separator: ':'
  }
}
